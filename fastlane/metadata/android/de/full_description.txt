Barcode Scanner ist eine kostenlose und quelloffene App, die es erlaubt, Barcodes zu lesen und zu erzeugen. Sie kann Informationen über Lebensmittel, Kosmetika, Bücher und Musik (CDs, Vinyls...) sammeln.

Die App unterstützt verschiedene Barcode-Formate:
- zweidimensionale Barcodes: QR Code, Data Matrix, PDF 417, AZTEC
- eindimensionale Barcodes: EAN 13, EAN 8, UPC A, UPC E, Code 128, Code 93, Code 39, Codabar, ITF

Sammeln Sie während des Scannens Informationen über ein Produkt:
- Lebensmittel mit Open Food Facts
- Kosmetik mit Open Beauty Facts
- Lebensmittel für Haustiere mit Open Pet Food Facts
- Bücher mit Open Library
- Musik-CDs, Vinyls... mit MusicBrainz

App-Funktionen:
- Richten Sie einfach die Kamera Ihres Smartphones auf einen Barcode und erhalten Sie sofort Informationen darüber. Sie können Barcodes auch in einem Bild auf Ihrem Smartphone scannen.
- Lesen Sie mit einem einfachen Scan Visitenkarten, fügen Sie neue Kontakte hinzu, fügen Sie neue Veranstaltungen zu Ihrer Agenda hinzu, öffnen Sie URLs oder stellen Sie sogar eine WLAN-Verbindung her.
- Scannen Sie Barcodes von Lebensmitteln, um dank der Datenbanken Open Food Facts und Open Beauty Facts Informationen über deren Inhaltsstoffe zu erhalten.
- Suchen Sie auf verschiedenen Websites wie Amazon oder Fnac nach Informationen über das gescannte Produkt.
- Behalten Sie mit dem Verlaufstool den Überblick über alle gescannten Barcodes.
- Erstellen Sie Ihre eigenen Barcodes.
- Passen Sie die Benutzeroberfläche mit verschiedenen Farben an, mit einem hellen oder einem dunklen Thema. Die App integriert die Funktionen von Android 12 und ermöglicht es, die Farben je nach Hintergrundbild anzupassen (Material You).
- Die Texte sind vollständig in Deutsch, Englisch, Spanisch, Französisch, Polnisch, Türkisch, Russisch und Chinesisch übersetzt.

Diese App respektiert Ihre Privatsphäre. Sie enthält keine Tracker und sammelt keine Daten.
